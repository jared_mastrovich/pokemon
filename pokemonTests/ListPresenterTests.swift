//
//  ListPresenterTests.swift
//  pokemonTests
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import XCTest
@testable import pokemon

class ListPresenterTests: XCTestCase {
    private let mockApiService = MockPokeApiService(responseDelay: 0)
    
    override func setUp() {
        
        super.setUp()
        
        // Set the mock service to have zero delay, so tests run faster
        ServiceProvider.pokeApiService = self.mockApiService
    }
    
    func testSearch() {
        let view = TestListView()
        let presenter = ListPresenter(view: view)
        presenter.getData()
        
        let totalRows = MockPokeApiService.fakeData.count
        
        // all rows should be displayed by default:
        
        XCTAssertEqual(totalRows, presenter.numberOfItems())
        
        // initiate a search to restrict the number of rows to one:
        
        presenter.searchTextChanged(to: presenter.item(at: 0).name)
        
        XCTAssertEqual(1, presenter.numberOfItems())
        
        // clear the search text to return to the original number of rows:
        
        presenter.searchTextChanged(to: "")
        
        XCTAssertEqual(totalRows, presenter.numberOfItems())
    }
    
    func testSelection() {
        let view = TestListView()
        let presenter = ListPresenter(view: view)
        
        // There should be no items until getData is called:
        XCTAssertEqual(0, presenter.numberOfItems())
        
        presenter.getData()
        
        // Now there should be rows:
        XCTAssertGreaterThan(presenter.numberOfItems(), 0)
        
        // Selecting an item should cause details to show:
        
        XCTAssertEqual(0, view.showDetailsCount)
        presenter.selectedItem(at: 0)
        XCTAssertEqual(1, view.showDetailsCount)
    }
    
    func testGetData() {
        let view = TestListView()
        let presenter = ListPresenter(view: view)
        
        // Calling getData() should:
        // 1. tell the UI to show the loading screen,
        // 2. tell the UI to hide the loading screen,
        // 3. tell the UI to update the table, or show an error
        
        XCTAssertEqual(0, view.showLoadingCount)
        XCTAssertEqual(0, view.hideLoadingCount)
        XCTAssertEqual(0, view.updateTableCount)
        XCTAssertEqual(0, view.showRetryableErrorCount)

        // First time around, we'll force an error:
        
        self.mockApiService.forceError = true
        presenter.getData()
        
        XCTAssertEqual(1, view.showLoadingCount)
        XCTAssertEqual(1, view.hideLoadingCount)
        XCTAssertEqual(0, view.updateTableCount)
        XCTAssertEqual(1, view.showRetryableErrorCount)
        
        guard let retryAction = view.lastRetryAction else {
            XCTFail("No retry action set after forcing API error")
            return
        }
        
        // Test the retry behaviour
        
        view.resetCounts()
        self.mockApiService.forceError = false
        retryAction()
        
        XCTAssertEqual(1, view.showLoadingCount)
        XCTAssertEqual(1, view.hideLoadingCount)
        XCTAssertEqual(1, view.updateTableCount)
        XCTAssertEqual(0, view.showRetryableErrorCount)
    }
}

fileprivate class TestListView: TestApiView, ListViewProtocol {
    var updateTableCount = 0
    var showDetailsCount = 0
    
    var lastShowDetailsItem: PokemonListItem?
    
    override func resetCounts() {
        
        super.resetCounts()
        
        self.updateTableCount = 0
        self.showDetailsCount = 0

        self.lastShowDetailsItem = nil
    }
    
    func updateTable() {
        self.updateTableCount += 1
    }
    
    func showDetails(for item: PokemonListItem) {
        self.lastShowDetailsItem = item
        self.showDetailsCount += 1
    }    
}
