//
//  TestApiView.swift
//  pokemonTests
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

class TestApiView: NSObject {

    var showLoadingCount = 0
    var hideLoadingCount = 0
    var showRetryableErrorCount = 0
    
    var lastRetryAction: (() -> Void)?
    
    func resetCounts() {
        self.showLoadingCount = 0
        self.hideLoadingCount = 0
        self.showRetryableErrorCount = 0
        
        self.lastRetryAction = nil
    }
    
    func showLoading() {
        self.showLoadingCount += 1
    }
    
    func hideLoading() {
        self.hideLoadingCount += 1
    }
    
    func showRetryableError(message: String, retryAction: @escaping () -> Void) {
        self.lastRetryAction = retryAction
        self.showRetryableErrorCount += 1
    }
}
