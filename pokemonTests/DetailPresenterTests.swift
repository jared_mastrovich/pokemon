//
//  DetailPresenterTests.swift
//  pokemonTests
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import XCTest
@testable import pokemon

class DetailPresenterTests: XCTestCase {
    private let mockApiService = MockPokeApiService(responseDelay: 0)
    
    override func setUp() {
        
        super.setUp()
        
        // Set the mock service to have zero delay, so tests run faster
        ServiceProvider.pokeApiService = self.mockApiService
    }

    func testGetData() {
        let mockItem = PokemonListItem(id: "1", name: "Mockachu")
        
        let view = TestDetailView()
        let presenter = DetailPresenter(view: view, for: mockItem)
        
        // Calling getDetails() should:
        // 1. tell the UI to show the loading screen,
        // 2. tell the UI to hide the loading screen,
        // 3. tell the UI to update the view, or show an error
        
        XCTAssertEqual(0, view.showLoadingCount)
        XCTAssertEqual(0, view.hideLoadingCount)
        XCTAssertEqual(0, view.showDetailsCount)
        XCTAssertEqual(0, view.showRetryableErrorCount)
        
        // First time around, we'll force an error:
        
        self.mockApiService.forceError = true
        presenter.getDetails()
        
        XCTAssertEqual(1, view.showLoadingCount)
        XCTAssertEqual(1, view.hideLoadingCount)
        XCTAssertEqual(0, view.showDetailsCount)
        XCTAssertEqual(1, view.showRetryableErrorCount)
        
        guard let retryAction = view.lastRetryAction else {
            XCTFail("No retry action set after forcing API error")
            return
        }
        
        // Test the retry behaviour
        
        view.resetCounts()
        self.mockApiService.forceError = false
        retryAction()
        
        XCTAssertEqual(1, view.showLoadingCount)
        XCTAssertEqual(1, view.hideLoadingCount)
        XCTAssertEqual(1, view.showDetailsCount)
        XCTAssertEqual(0, view.showRetryableErrorCount)
    }
}

fileprivate class TestDetailView: TestApiView, DetailViewProtocol {
    var showDetailsCount = 0
    
    func showDetails(for: PokemonDetails) {
        self.showDetailsCount += 1
    }
    
    override func resetCounts() {
        
        super.resetCounts()
        
        self.showDetailsCount = 0
    }
}
