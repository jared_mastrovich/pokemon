//
//  PokemonListItem.swift
//  pokemon
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

struct PokemonListItem {

    let id: String
    let name: String
}
