//
//  PokemonDetails.swift
//  pokemon
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

struct PokemonDetails {

    let name: String
    let imageUrl: URL?
    let weight: Int
    let height: Int
}
