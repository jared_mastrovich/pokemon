//
//  PokeApiServiceProtocol.swift
//  pokemon
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

protocol PokeApiServiceProtocol {

    func getNames(_ completion: @escaping ([PokemonListItem]?, Error?) -> Void)

    func getInfo(for: PokemonListItem, _ completion: @escaping (PokemonDetails?, Error?) -> Void)
}
