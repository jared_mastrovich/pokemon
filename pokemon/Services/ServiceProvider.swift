//
//  ServiceProvider.swift
//  pokemon
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

class ServiceProvider: NSObject {
    #if MOCK
    static var pokeApiService: PokeApiServiceProtocol = MockPokeApiService(responseDelay: 1)
    #else
    static var pokeApiService: PokeApiServiceProtocol = PokeApiService()
    #endif
}
