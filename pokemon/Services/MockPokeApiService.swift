//
//  MockPokeApiService.swift
//  pokemon
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

class MockPokeApiService: NSObject, PokeApiServiceProtocol {
    
    static let forcedErrorDomain = "MockPokeApiService Forced Error"
    var forceError: Bool = false
    
    private let responseDelay: Int
    
    init(responseDelay: Int) {
        
        self.responseDelay = responseDelay
        super.init()
    }
    
    static let fakeData = [
        PokemonListItem(id: "1", name: "Mockachu"),
        PokemonListItem(id: "2", name: "Fakeachu"),
        PokemonListItem(id: "3", name: "Bogusaur")
    ]
    
    func getNames(_ completion: @escaping ([PokemonListItem]?, Error?) -> Void) {
        
        if let forcedError = forcedError() {
            executeAfterDelay {
                completion(nil, forcedError)
            }

            return
        }

        executeAfterDelay {
            completion(MockPokeApiService.fakeData, nil)
        }
    }
    
    func getInfo(for: PokemonListItem, _ completion: @escaping (PokemonDetails?, Error?) -> Void) {
        
        if let forcedError = forcedError() {
            executeAfterDelay {
                completion(nil, forcedError)
            }
            
            return
        }
        
        guard let fakeUrl = URL(string: "https://via.placeholder.com/40/000000/00ff00/.png") else {
            fatalError("This will only happen if there is a typo in the image above")
        }

        let fakeData = PokemonDetails(name: "Mockachu",
                                      imageUrl: fakeUrl,
                                      weight: 123,
                                      height: 4)
        
        executeAfterDelay {
            completion(fakeData, nil)
        }
    }
    
    private func forcedError() -> Error? {
        
        if self.forceError {
            return NSError(domain: MockPokeApiService.forcedErrorDomain,
                           code: 0,
                           userInfo: nil)
        }
        
        return nil
    }
    
    // Return mock response on after delay on background thread for two reasons:
    // 1. The delay allows us to observe any loading behaviour
    // 2. Execution on the background thread allows us to simulate any
    //    "UI modified on background thread" issues
    
    private func executeAfterDelay(operation: @escaping () -> Void) {
        
        // If there is no delay specified, execute the operation on the same thread.
        
        if self.responseDelay == 0 {
            operation()
            return
        }
        
        let dispatchAt = DispatchTime.now() + .seconds(self.responseDelay)
        DispatchQueue.global(qos: .background).asyncAfter(deadline: dispatchAt) {
            operation()
        }
    }
}
