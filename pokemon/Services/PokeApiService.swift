//
//  PokeApiService.swift
//  pokemon
//
//  Created by Jared Mastrovich on 24/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

class PokeApiService: NSObject, PokeApiServiceProtocol {
    
    func getNames(_ completion: @escaping ([PokemonListItem]?, Error?) -> Void) {
        
        guard let url = URL(string: "https://pokeapi.co/api/v2/pokemon/?limit=1000") else {
            fatalError("This will only happen if there is a typo in the URL")
        }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            guard
                let data = data,
                let names = try? JSONDecoder().decode(ListApiResponse.List.self, from: data) else {
                    completion(nil, error)
                    return
            }
            
            var result: [PokemonListItem] = []
            
            for pokemon in names.results {
                // extract id component from url
                guard
                    let url = URL(string: pokemon.url),
                    let id = url.pathComponents.last else {
                        // url in unexpected format!
                        continue
                }
                
                let item = PokemonListItem(id: id, name: pokemon.name)
                result.append(item)
            }
            
            completion(result, nil)
        }.resume()
    }
    
    func getInfo(for item: PokemonListItem, _ completion: @escaping (PokemonDetails?, Error?) -> Void) {
        
        guard let url = URL(string: "https://pokeapi.co/api/v2/pokemon/" + item.id) else {
            fatalError("This will only happen if there is a typo in the URL")
        }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            guard
                let data = data,
                let detail = try? JSONDecoder().decode(DetailApiResponse.self, from: data) else {
                    completion(nil, error)
                    return
            }
            
            var imageUrl: URL?
            
            if let imageUrlString = detail.imageUrl {
                imageUrl = URL(string: imageUrlString)
            }
            
            let result = PokemonDetails(name: detail.name,
                                        imageUrl: imageUrl,
                                        weight: detail.weight,
                                        height: detail.height)

            completion(result, nil)
        }.resume()
    }
}

fileprivate struct ListApiResponse {
    
    fileprivate struct List: Decodable {
        
        let results: [NameResult]
    }
    
    fileprivate struct NameResult: Decodable {
        
        let name: String
        let url: String
    }
}

fileprivate struct DetailApiResponse: Decodable {
    
    let name: String
    let weight: Int
    let height: Int
    let imageUrl: String?
    
    // Because the imageUrl is nested at sprites.front_default,
    // we need to manually describe decoding of the JSON data:
    
    enum CodingKeys: String, CodingKey {
        
        case name
        case weight
        case height
        case sprites
    }
    
    enum SpritesKeys: String, CodingKey {
        
        case frontDefault = "front_default"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.name = try values.decode(String.self, forKey: .name)
        self.weight = try values.decode(Int.self, forKey: .weight)
        self.height = try values.decode(Int.self, forKey: .height)
        
        if let sprites = try? values.nestedContainer(keyedBy: SpritesKeys.self,
                                                     forKey: .sprites),
            let imageUrl = try? sprites.decode(String.self, forKey: .frontDefault) {
            self.imageUrl = imageUrl
        } else {
            self.imageUrl = nil
        }
    }
}
