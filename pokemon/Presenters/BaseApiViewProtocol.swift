//
//  BaseApiViewProtocol.swift
//  pokemon
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

protocol BaseApiViewProtocol: class {
    
    func showLoading()
    
    func hideLoading()
    
    func showRetryableError(message: String, retryAction: @escaping () -> Void)
}
