//
//  ListPresenter.swift
//  pokemon
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

protocol ListViewProtocol: BaseApiViewProtocol {
    
    func updateTable()

    func showDetails(for item: PokemonListItem)
}

class ListPresenter: NSObject {
    
    private let apiService = ServiceProvider.pokeApiService
    private let view: ListViewProtocol
    private var data: [PokemonListItem]?
    private var filteredData: [PokemonListItem]?
    
    init(view: ListViewProtocol) {
        
        self.view = view
        super.init()
    }
 
    func getData() {
        
        self.view.showLoading()
        
        self.apiService.getNames { response, _ in
            self.view.hideLoading()
            
            guard let pokemonList = response else {
                self.view.showRetryableError(message: "Something went wrong!",
                                             retryAction: { self.getData() })
                return
            }
            
            self.data = pokemonList
            self.view.updateTable()
        }
    }
    
    func numberOfItems() -> Int {
        
        guard let currentSet = self.filteredData ?? self.data else {
            return 0
        }
        
        return currentSet.count
    }
    
    func item(at index: Int) -> PokemonListItem {
        
        guard let data = self.filteredData ?? self.data else {
            fatalError("Attempt to retrieve item when no data is available. Call getData() first.")
        }
        
        return data[index]
    }
    
    func selectedItem(at index: Int) {
        
        let selected = item(at: index)
        self.view.showDetails(for: selected)
    }
    
    func searchTextChanged(to searchFor: String) {
        
        if searchFor.trimmingCharacters(in: .whitespaces).isEmpty {
            // user has cleared the search field, can the filter:
            self.filteredData = nil
        } else {
            // filter using the search string:
            self.filteredData = self.data?.filter({ pokemon -> Bool in
                return pokemon.name.localizedCaseInsensitiveContains(searchFor)
            })
        }
        
        self.view.updateTable()
    }
}
