//
//  DetailPresenter.swift
//  pokemon
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

protocol DetailViewProtocol: BaseApiViewProtocol {
    
    func showDetails(for: PokemonDetails)
}

class DetailPresenter: NSObject {
    
    private let apiService = ServiceProvider.pokeApiService
    private let view: DetailViewProtocol
    private let item: PokemonListItem

    init(view: DetailViewProtocol, for item: PokemonListItem) {
        
        self.view = view
        self.item = item
        super.init()
    }
    
    func getDetails() {
        self.view.showLoading()
        
        self.apiService.getInfo(for: self.item) { response, _ in
            self.view.hideLoading()
            
            guard let pokemon = response else {
                self.view.showRetryableError(message: "Something went wrong!",
                                             retryAction: { self.getDetails() })
                return
            }
            
            self.view.showDetails(for: pokemon)
        }
    }
}
