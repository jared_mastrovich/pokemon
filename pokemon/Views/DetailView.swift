//
//  DetailView.swift
//  pokemon
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

class DetailView: BaseApiView {
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var label: UILabel!
    
    private var presenter: DetailPresenter?

    func setup(with item: PokemonListItem) {
        
        self.title = item.name
        self.presenter = DetailPresenter(view: self, for: item)
    }

    override func viewDidLayoutSubviews() {
        
        if hasCompletedInitialLayout() == false {
            self.presenter?.getDetails()
        }

        super.viewDidLayoutSubviews()        
    }
    
    override var canDismissOnRetry: Bool {
        
        return true
    }
}

extension DetailView: DetailViewProtocol {
    
    func showDetails(for pokemon: PokemonDetails) {
        
        if let imageUrl = pokemon.imageUrl {
            self.imageView.load(from: imageUrl)
        }
        
        DispatchQueue.main.async {
            self.label.text = "Weight: \(pokemon.weight)\n\nHeight: \(pokemon.height)"
        }
    }
}
