//
//  ListView.swift
//  pokemon
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

class ListView: BaseApiView {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var presenter: ListPresenter?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
     
        self.title = "Pokemon"
        self.presenter = ListPresenter(view: self)
    }
    
    override func viewDidLayoutSubviews() {
        
        if hasCompletedInitialLayout() == false {
            self.presenter?.getData()
        }

        super.viewDidLayoutSubviews()
    }
}

extension ListView: ListViewProtocol {
    
    func updateTable() {
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showDetails(for item: PokemonListItem) {
        
        self.performSegue(withIdentifier: "DetailView", sender: item)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if
            let detailView = segue.destination as? DetailView,
            let pokemon = sender as? PokemonListItem {
            detailView.setup(with: pokemon)
        }
    }
}

extension ListView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let presenter = self.presenter else {
            fatalError("Attempt to load TableView before presenter is set")
        }
        
        return presenter.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let item = self.presenter?.item(at: indexPath.row) else {
            fatalError("Unable to retrieve data for row \(indexPath.row)")
        }
        
        let cell = UITableViewCell()
        
        cell.textLabel?.text = item.name
        
        return cell
    }
}

extension ListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.presenter?.selectedItem(at: indexPath.row)
    }
}

extension ListView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presenter?.searchTextChanged(to: searchText)
    }
}
