//
//  BaseApiView.swift
//  pokemon
//
//  Created by Jared Mastrovich on 23/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

class BaseApiView: UIViewController {

    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    private var viewDidLayoutSubviewsCount = 0

    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        self.viewDidLayoutSubviewsCount += 1
    }
    
    func hasCompletedInitialLayout() -> Bool {
        
        return self.viewDidLayoutSubviewsCount > 0
    }
    
    var canDismissOnRetry: Bool {
        
        return false
    }
}

extension BaseApiView: BaseApiViewProtocol {

    func showLoading() {
        
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    
    func hideLoading() {
        
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    
    func showRetryableError(message: String, retryAction: @escaping () -> Void) {
        
        let dialog = UIAlertController(title: nil,
                                       message: message,
                                       preferredStyle: .alert)
        
        if self.canDismissOnRetry {
            dialog.addAction(UIAlertAction(title: "Cancel",
                                           style: .default,
                                           handler: { _ in
                                            self.navigationController?.popViewController(animated: true)
            }))
        }
        
        dialog.addAction(UIAlertAction(title: "Try again",
                                       style: .default,
                                       handler: { _ in
                                        retryAction()
        }))
        
        present(dialog, animated: true, completion: nil)
    }
}
