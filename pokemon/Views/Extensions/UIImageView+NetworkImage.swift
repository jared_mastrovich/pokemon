//
//  UIImageView+NetworkImage.swift
//  pokemon
//
//  Created by Jared Mastrovich on 24/06/18.
//  Copyright © 2018 Jared Mastrovich. All rights reserved.
//

import UIKit

extension UIImageView {
    func load(from url: URL) {
        URLSession.shared.dataTask(with: url) { data, _, _ in
            guard
                let data = data,
                let image = UIImage(data: data) else {
                return
            }
            
            DispatchQueue.main.async {
                self.backgroundColor = .clear
                self.image = image
            }
        }.resume()
    }
}
